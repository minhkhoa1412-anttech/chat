import React from 'react'
import {createStackNavigator, createAppContainer} from 'react-navigation'
import LoginScreen from '../screens/Login/LoginScreen'
import SignUpScreen from '../screens/SignUp/SignUpScreen'
import HomeScreen from '../screens/Home/HomeScreen'
import SplashScreen from '../screens/Splash/SplashScreen'
import AHeader from '../components/AHeader/AHeader'
import SearchPeopleScreen from '../screens/Search/SearchPeopleScreen'
import SearchScreen from '../screens/Search/SearchScreen'

const MainStack = createStackNavigator(
  {
    Splash: {
      screen: SplashScreen,
      navigationOptions: {
        header: null
      }
    },
    Login: {
      screen: LoginScreen,
      navigationOptions: {
        header: null
      }
    },
    SignUp: {
      screen: SignUpScreen,
      navigationOptions: {
        header: null
      }
    },
    Main: {
      screen: HomeScreen,
      navigationOptions: {
        header: null
      }
    },
    SearchPeople: {
      screen: SearchPeopleScreen,
      navigationOptions: {
        header: null
      }
    },
    Search: {
      screen: SearchScreen,
      navigationOptions: {
        header: null
      }
    }
  }
)

export default createAppContainer(MainStack)
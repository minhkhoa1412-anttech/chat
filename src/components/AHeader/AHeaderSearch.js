import React, { Component } from 'react'
import { View, Text, StyleSheet, TextInput, TouchableOpacity } from 'react-native'
import { getStatusBarHeight } from 'react-native-status-bar-height'
import Icon from 'react-native-vector-icons/Ionicons'
import { metrics } from '../../vars/metrics'
import { platforms } from '../../vars/platforms'
import { colors } from '../../vars/colors'
import { strings } from '../../vars/strings'

class AHeaderSearch extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.goBack()
            }}>
            <Icon style={styles.icon} name={`${platforms.isAndroid ? 'md' : 'ios'}-arrow-back`} size={30} color={colors.black}/>
          </TouchableOpacity>
          <TextInput style={styles.textInput} placeholder={strings.searchPeople} autoFocus/>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    shadowOpacity: 10,
    elevation: 10,
    backgroundColor: 'white',
    height: 60 + getStatusBarHeight(true),
    width: metrics.screen_width
  },
  header: {
    flex: 1,
    height: 75,
    marginTop: getStatusBarHeight(true),
    flexDirection: 'row',
    paddingHorizontal: 18,
    alignItems: 'center'
  },
  icon: {
    marginStart: 16
  },
  textInput: {
    flex: 1,
    marginStart: 10,
    paddingStart: 10,
    fontSize: 17,
    fontWeight: 'bold',
    color: colors.colorTextGray
  }
})

export default AHeaderSearch
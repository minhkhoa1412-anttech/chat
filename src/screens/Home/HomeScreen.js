import React, { Component } from 'react'
import { View, StyleSheet } from 'react-native'

import TabNavigator from '../../navigator/TabNavigator'
import AHeader from '../../components/AHeader/AHeader'

class HomeScreen extends Component {

  render() {
    return (
      <View style={styles.container}>
        <AHeader/>
        <TabNavigator screenProps={this.props.navigation}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
})

export default HomeScreen
import { combineEpics, ofType } from 'redux-observable'
import { mergeMap, map, catchError } from 'rxjs/operators'
import * as firebase from 'react-native-firebase'
import { signUpSuccess } from '../actions'
import { constants } from '../vars/constants'
import { getUserSuccess } from '../actions'
import { Observable } from 'rxjs'
import { strings } from '../vars/strings'

const signUpEvent$ = (action) => new Observable(observer => {
  firebase
    .auth()
    .createUserWithEmailAndPassword(action.email, action.password)
    .then((user) => {
      if (action.uri.substring(0, 4) === 'http') {
        return Promise.all([
          Promise.resolve(user),
          Promise.resolve(strings.avatarURL)
        ])
      } else {
        return Promise.all([
          Promise.resolve(user),
          firebase.storage().ref(user.user.uid).child(Date.now() + '_' + action.filename).putFile(action.uri)
        ])
      }
    })
    .then(([user, image]) => {
      const newUser = {
        userId: user.user.uid,
        userName: action.username,
        userEmail: action.email,
        userAvatar: typeof image === 'object' ? image.downloadURL : image,
        userStatus: false,
        userFriend: []
      }
      return firebase.database().ref('Users/' + `${newUser.userId}`).set(newUser)
    })
    .then((data) => {
      observer.next(data)
      observer.complete()
    })
    .catch((err) => observer.error(err))
})

const signUp = action$ => action$.pipe(
  ofType(constants.SIGN_UP),
  mergeMap((action) => {
    return signUpEvent$(action).pipe(
      map(() => {
        return signUpSuccess()
      }),
      catchError((err) => {
        alert(err)
      })
    )
  })
)

const getUserEvent$ = (action) => {
  return new Observable(observer => {
    firebase.database().ref('Users/' + `${action.userId}`).on('value', (snapshot) => {
      observer.next(snapshot.val())
    })
  })
}

const getUser = action$ => action$.pipe(
  ofType(constants.GET_USER),
  mergeMap((action) => {
    return getUserEvent$(action).pipe(
      map((value) => {
        return getUserSuccess(value)
      })
    )
  })
)

const signIn = action$ => action$.pipe(
  ofType(constants.SIGN_IN),
  mergeMap((action) => {
    firebase.auth().signInWithEmailAndPassword(action.userEmail, action.userPassword).then(() => {})
    return []
  })
)

export const rootEpic = combineEpics(
  signUp,
  signIn,
  getUser
)
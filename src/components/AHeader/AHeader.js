import React, { Component } from 'react'
import { View, Image, StyleSheet, Text, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import { getStatusBarHeight } from 'react-native-status-bar-height'
import Icon from 'react-native-vector-icons/Ionicons'
import { metrics } from '../../vars/metrics'

class AHeader extends Component {
  render() {
    const { user } = this.props

    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={styles.containAvatar}>
            <Image style={styles.avatar} source={{ uri: user.userAvatar }}/>
            <Text style={styles.text}>Chat</Text>
          </View>
          <View style={styles.containMenu}>
            <TouchableOpacity>
              <Icon name={'ios-create'} size={30} color='#000'/>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop: getStatusBarHeight(true),
    height: 85,
    backgroundColor: 'white',
    width: metrics.screen_width
  },
  header: {
    flexDirection: 'row',
    backgroundColor: 'white',
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 18
  },
  containAvatar: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  avatar: {
    width: 40,
    height: 40,
    borderRadius: 20
  },
  text: {
    fontSize: 25,
    fontWeight: 'bold',
    marginStart: 18
  },
  containMenu: {
    flexDirection: 'row'
  }
})

const mapStateToProps = (state) => ({
  user: state.authReducer.user
})

export default connect(mapStateToProps)(AHeader)
import React, { Component } from 'react'
import { View, Text, StyleSheet } from 'react-native'

class ItemUser extends Component {
  render() {
    const { item } = this.props

    return (
      <View style={styles.container}>
        <Text>{item.userName}</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})

export default ItemUser
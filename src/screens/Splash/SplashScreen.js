import React, { Component } from 'react'
import { View, StyleSheet, ActivityIndicator } from 'react-native'
import { connect } from 'react-redux'
import { getUser, clearUser } from '../../actions'
import { colors } from '../../vars/colors'
import * as firebase from 'react-native-firebase'

class LoginScreen extends Component {
  componentDidMount() {
    firebase.auth().onAuthStateChanged(async user => {
      if (user) {
        this.props.getUser(user.uid)
      } else {
        this.props.clearUser()
      }
      await this.props.navigation.navigate(user ? 'Main' : 'Login')
    })
  }

  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator size='large' color={colors.white} animating={true}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
})

const mapDispatchToProps = (dispatch) => ({
  getUser: (userId) => {
    dispatch(getUser(userId))
  },
  clearUser: () => {
    dispatch(clearUser())
  }
})

export default connect(null, mapDispatchToProps)(LoginScreen)
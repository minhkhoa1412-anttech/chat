import { createBottomTabNavigator, createAppContainer } from 'react-navigation'
import Ripple  from 'react-native-material-ripple'
import { TouchableOpacity } from 'react-native'

import ChatScreen from '../screens/Chat/ChatScreen'
import FriendScreen from '../screens/Friends/FriendScreen'
import UserScreen from '../screens/User/UserScreen'
import { colors } from '../vars/colors'
import React from 'react'
import Icon from 'react-native-vector-icons/Ionicons'

const tabNavigator = createBottomTabNavigator(
  {
    Chat: ChatScreen,
    Friend: FriendScreen,
    User: UserScreen
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state
        let IconComponent = Icon
        let iconName
        if (routeName === 'Chat') {
          iconName = `ios-chatbubbles`
        } else if (routeName === 'Friend') {
          iconName = `md-contacts`
        } else if (routeName === 'User') {
          iconName = `md-contact`
        }
        return <IconComponent name={iconName} size={26} color={tintColor}/>
      }
    }),
    initialRouteName: 'Chat',
    tabBarOptions: {
      activeTintColor: colors.black,
      inactiveTintColor: colors.backgroundGray,
      activeBackgroundColor: colors.white,
      showLabel: false,
      style: {
      },
      tabStyle: {
        alignItems: 'center'
      },
    }
  }
)

export default createAppContainer(tabNavigator)
export const strings = {
  email: 'Email',
  username: 'Username',
  password: 'Password',
  forgotPassword: 'Forgot your password?',
  login: 'log in',
  dontHaveAccount: `Don't have a account? `,
  signUp: 'Sign up',
  signUpSuccess: 'Sign up successfully',
  enterUserAndPass: 'Please enter email and password',
  takePhoto: 'take photo',
  users: 'Users',
  avatarURL: 'https://scontent.fsgn3-1.fna.fbcdn.net/v/t1.0-9/49783381_1032860776897619_948737981965926400_n.jpg?_nc_cat=111&_nc_oc=AQnLm0DdAs1HIv-eqR5VQdukaKqv50vpB8RkzL1YYPG5Y5zK-MFD-YsjqoHxemmvaDHnNn9EyFI7DaimJUTLlCzr&_nc_ht=scontent.fsgn3-1.fna&oh=4357ba23812a2be8bfba65aae7747b08&oe=5CC42A86',
  search: 'Search',
  searchPeople: 'Search people'
}
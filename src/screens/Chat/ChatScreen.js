import React, {Component} from 'react'
import {View, StyleSheet, ScrollView} from 'react-native'
import ASearchBar from '../../components/ASearchBar/ASearchBar'
import { strings } from '../../vars/strings'

export default class ChatScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          <View>
            <ASearchBar search={strings.search}/>
          </View>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
})
import React, {Component} from 'react'
import {View, StyleSheet, ScrollView, TouchableOpacity, Text} from 'react-native'
import ASearchBar from '../../components/ASearchBar/ASearchBar'
import { strings } from '../../vars/strings'

export default class FriendScreen extends Component {
  moveScreen = () => {
    this.props.screenProps.navigate('SearchPeople')
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          <View>
            <ASearchBar search={strings.searchPeople} onPress={this.moveScreen}/>
          </View>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
})
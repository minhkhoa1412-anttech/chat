import React, {Component} from 'react'
import MainNavigator from './navigator/MainNavigator'
import {Provider} from 'react-redux'
import configureStore from './store'

export default class Application extends Component {
  render() {
    return (
      <Provider store={configureStore()}>
        <MainNavigator/>
      </Provider>
    )
  }
}
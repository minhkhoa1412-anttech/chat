import React, { Component } from 'react'
import { View, FlatList, StyleSheet, Text } from 'react-native'
import { connect } from 'react-redux'

import AHeaderSearch from '../../components/AHeader/AHeaderSearch'
import ItemUser from './Components/ItemUser'

class SearchPeopleScreen extends Component {

  render() {
    const { listUsers } = this.props

    return (
      <View style={styles.container}>
        <AHeaderSearch navigation={this.props.navigation}/>
        <FlatList
          keyExtractor={(item) => item.userId}
          data={listUsers}
          renderItem={({ item, index }) => (
            <ItemUser item={item}/>
          )}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
})

const mapStateToProps = (state) => ({
  listUsers: state.authReducer.listUsers
})

export default connect(mapStateToProps)(SearchPeopleScreen)
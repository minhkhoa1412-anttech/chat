import React, { Component } from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import { metrics } from '../../vars/metrics'
import { strings } from '../../vars/strings'
import { colors } from '../../vars/colors'
import Ripple from 'react-native-material-ripple'
import Icon from 'react-native-vector-icons/Ionicons'

class ASearchBar extends Component {
  render() {
    const { search, onPress } = this.props

    return (
      <View style={styles.container}>
        <TouchableOpacity
          style={styles.containSearch}
          onPress={() => {
            onPress()
          }}>
          <View style={styles.search}>
            <Icon name={'md-search'} size={24} color={colors.colorGray}/>
            <Text style={styles.text}>{search}</Text>
          </View>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    height: 65,
    paddingVertical: 10,
    paddingHorizontal: 16,
    width: metrics.screen_width
  },
  containSearch: {
    paddingVertical: 10,
    paddingStart: 15,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 20,
    backgroundColor: colors.backgroundGray
  },
  search: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  text: {
    color: colors.colorTextGray,
    fontSize: 17,
    marginStart: 15
  }
})

export default ASearchBar
import React, { Component } from 'react'
import { View, Text, TouchableOpacity, StyleSheet, Image, ActivityIndicator } from 'react-native'
import { connect } from 'react-redux'

import { TextField } from 'react-native-material-textfield'
import { strings } from '../../vars/strings'
import LinearGradient from 'react-native-linear-gradient'
import Icons from 'react-native-vector-icons/Ionicons'
import { getStatusBarHeight } from 'react-native-status-bar-height'
import ImagePicker from 'react-native-image-crop-picker'
import { colors } from '../../vars/colors'
import { metrics } from '../../vars/metrics'
import { platforms } from '../../vars/platforms'
import { signUp } from '../../actions'

class SignUpScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading: false,
      email: '',
      password: '',
      username: '',
      source: strings.avatarURL,
      filename: ''
    }
  }

  componentDidMount() {

  }

  signUp = (email, username, password, uri, filename) => {
    this.setState({ isLoading: true })

    if (email.trim() !== '' || password.trim() !== '') {
      this.props.signUp(email, username, password, uri, filename)
    } else {
      alert(strings.enterUserAndPass)
    }
  }

  pickImage = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 300,
      cropping: true
    }).then(image => {
      this.setState({ source: image.path, filename: image.filename })
    })
  }

  render() {
    const { email, password, username, source, filename } = this.state
    const { isSigningUp } = this.props

    return (
      <View style={styles.container}>
        <TouchableOpacity
          onPress={() => {
            this.props.navigation.pop()
          }}
          style={[styles.backButton, { top: platforms.isAndroid ? 12 : 10 + getStatusBarHeight() }]}>
          <Icons name={`${platforms.isAndroid ? 'md' : 'ios'}-arrow-back`} size={30}
                 color={colors.colorAccent}/>
        </TouchableOpacity>

        <View style={styles.containAvatar}>
          <Image
            style={styles.avatar}
            source={{ uri: this.state.source }}/>

          <TouchableOpacity
            onPress={() => {
              this.pickImage()
            }}>
            <LinearGradient
              style={styles.buttonTakePhoto}
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              colors={[colors.blueGradient, colors.cyanGradient]}>
              <View style={styles.innerButtonPhoto}>
                <Text>{strings.takePhoto.toUpperCase()}</Text>
              </View>
            </LinearGradient>
          </TouchableOpacity>
        </View>

        <View style={styles.textInput}>
          <TextField
            autoCapitalize='none'
            labelFontSize={14}
            label={strings.email}
            value={email}
            onChangeText={(email) => this.setState({ email: email })}
          />
        </View>
        <View style={styles.textInput}>
          <TextField
            labelFontSize={14}
            label={strings.username}
            value={username}
            onChangeText={(username) => this.setState({ username })}
          />
        </View>
        <View style={styles.textInput}>
          <TextField
            secureTextEntry
            textContentType='password'
            labelFontSize={14}
            label={strings.password}
            value={password}
            onChangeText={(password) => this.setState({ password })}
          />
        </View>
        <TouchableOpacity
          onPress={() => {
            this.signUp(email, username, password, source, filename)
          }}>
          <LinearGradient
            style={styles.button}
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 0 }}
            colors={[colors.blueGradient, colors.cyanGradient]}>
            {isSigningUp ? <ActivityIndicator color={colors.white}/> :
              <Text style={styles.textButton}>{strings.signUp.toUpperCase()}</Text>}
          </LinearGradient>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: '7.5%'
  },
  textInput: {
    width: '100%',
    marginTop: 5
  },
  button: {
    height: 45,
    width: metrics.screen_width * 90 / 100,
    marginTop: 40,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20
  },
  textButton: {
    fontSize: 20,
    color: colors.white
  },
  backButton: {
    position: 'absolute',
    left: 16
  },
  avatar: {
    width: metrics.screen_width / 4,
    height: metrics.screen_width / 4,
    borderRadius: metrics.screen_width / 8,
    borderWidth: 2,
    borderColor: colors.colorAccent
  },
  containAvatar: {
    width: metrics.screen_width,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    marginBottom: 40
  },
  buttonTakePhoto: {
    padding: 2,
    borderRadius: 20,
  },
  innerButtonPhoto: {
    borderRadius: 20,
    paddingHorizontal: 40,
    paddingVertical: 5,
    backgroundColor: colors.white
  }
})

const mapStateToProps = (state) => {
  return {
    isSigningUp: state.authReducer.isSigningUp
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    signUp: (email, username, password, uri, filename) => {
      dispatch(signUp(email, username, password, uri, filename))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUpScreen)
import React, {Component} from 'react'
import {View, StyleSheet, ScrollView, Text} from 'react-native'
import * as firebase from 'react-native-firebase'
import ASearchBar from '../../components/ASearchBar/ASearchBar'
import { TouchableRipple } from 'react-native-paper'

export default class UserScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          <View>
            <TouchableRipple
              onPress={() => {
                firebase.auth().signOut()
              }}>
              <Text>Sign out</Text>
            </TouchableRipple>
            <ASearchBar/>
          </View>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
})
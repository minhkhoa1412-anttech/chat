import { constants } from '../vars/constants'

export const signIn = (userEmail, userPassword) => ({
  type: constants.SIGN_IN,
  userEmail: userEmail,
  userPassword: userPassword
})

export const signUp = (email, username, password, uri, filename) => ({
  type: constants.SIGN_UP,
  email: email,
  username: username,
  password: password,
  uri: uri,
  filename: filename
})

export const signUpSuccess = () => ({
  type: constants.SIGN_UP_SUCCESS
})

export const getUser = (userId) => ({
  type: constants.GET_USER,
  userId: userId

})

export const getUserSuccess = (payload) => ({
  type: constants.GET_USER_SUCCESS,
  payload: payload
})

export const clearUser = () => ({
  type: constants.CLEAR_USER
})
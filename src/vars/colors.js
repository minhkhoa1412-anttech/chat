export const colors = {
  black: '#000',
  backgroundGray: '#e0e0e0',
  colorTextGray: '#585858',
  colorAccent: '#2b82ff',
  colorGray: 'gray',
  white: '#fff',
  orange: '#ff5858',
  pink: '#f857a6',
  blueGradient: '#4facfe',
  cyanGradient: '#00f2fe',
}
import React, { Component } from 'react'
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native'
import { TextField } from 'react-native-material-textfield'
import { connect } from 'react-redux'
import { strings } from '../../vars/strings'
import { colors } from '../../vars/colors'
import { metrics } from '../../vars/metrics'
import LinearGradient from 'react-native-linear-gradient'
import { signIn } from '../../actions'

class LoginScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      password: ''
    }
  }

  render() {
    const { email, password } = this.state
    const { signIn } = this.props

    return (
      <View style={styles.container}>
        <View style={styles.textInput}>
          <TextField
            autoCapitalize='none'
            labelFontSize={14}
            label={strings.email}
            value={email}
            onChangeText={(email) => this.setState({ email })}
          />
        </View>
        <View style={styles.textInput}>
          <TextField
            secureTextEntry
            textContentType='password'
            labelFontSize={14}
            label={strings.password}
            value={password}
            onChangeText={(password) => this.setState({ password })}
          />
        </View>
        <TouchableOpacity style={styles.containForget}>
          <Text style={styles.text}>{strings.forgotPassword}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            if (email !== '' && password !== '') {
              signIn(email, password)
            }
          }}>
          <LinearGradient
            style={styles.button}
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 0 }}
            colors={[colors.blueGradient, colors.cyanGradient]}>
            <Text style={styles.textButton}>{strings.login.toUpperCase()}</Text>
          </LinearGradient>
        </TouchableOpacity>
        <View style={styles.containerSignUp}>
          <Text style={styles.textForgot}>{strings.dontHaveAccount}</Text>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('SignUp')
            }}>
            <Text style={styles.textSignUp}>{strings.signUp}</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: '7.5%'
  },
  textInput: {
    width: '100%',
    marginTop: 5
  },
  containForget: {
    alignSelf: 'flex-end'
  },
  text: {
    marginVertical: 20,
    fontSize: 16,
    color: colors.colorAccent
  },
  button: {
    width: metrics.screen_width * 90 / 100,
    marginTop: 5,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20
  },
  containerSignUp: {
    marginTop: 50,
    flexDirection: 'row',
    alignItems: 'center'
  },
  textButton: {
    fontSize: 20,
    color: colors.white
  },
  textForgot: {
    fontSize: 14
  },
  textSignUp: {
    fontSize: 14,
    fontWeight: 'bold',
    color: colors.colorAccent
  }
})

const mapDispatchToProps = (dispatch) => {
  return {
    signIn: (email, password) => {
      dispatch(signIn(email, password))
    }
  }
}

export default connect(null, mapDispatchToProps)(LoginScreen)
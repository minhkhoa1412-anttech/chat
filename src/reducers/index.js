import { constants } from '../vars/constants'

const initialState = {
  isLogged: false,
  isLoggingIn: false,
  isSigningUp: false,
  listUsers: [
    {
      userId: 'a',
      userName: 'meo',
      userEmail: 'meo@gmail.com',
      userAvatar: '',
      userStatus: false,
      userFriend: []
    },
    {
      userId: 'b',
      userName: 'meo 1',
      userEmail: 'meo1@gmail.com',
      userAvatar: '',
      userStatus: false,
      userFriend: []
    }
  ],
  user: {
    userId: '',
    userName: '',
    userEmail: '',
    userAvatar: '',
    userStatus: false,
    userFriend: []
  }
}

export const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case constants.SIGN_UP:
      return {
        ...state,
        isSigningUp: true
      }
    case constants.SIGN_UP_SUCCESS:
      return {
        ...state,
        isSigningUp: false,
        isLogged: true,
      }
    case constants.GET_USER_SUCCESS:
      if (action.payload !== null) {
        return {
          ...state,
          user: {
            userId: action.payload.userId,
            userName: action.payload.userName,
            userEmail: action.payload.userEmail,
            userAvatar: action.payload.userAvatar,
            userStatus: action.payload.userStatus,
            userFriend: action.payload.userFriend ? action.payload.userFriend : []
          }
        }
      } else {
        return {
          ...state
        }
      }
    case constants.CLEAR_USER:
      return {
        ...state,
        user: {
          userId: '',
          userName: '',
          userEmail: '',
          userAvatar: '',
          userStatus: false,
          userFriend: []
        }
      }
    default:
      return state
  }
}